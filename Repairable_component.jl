# enumeration to show the component state
@enum State active=1 inactive=2 faulty=3

HOURS_TO_SECONDS = 3600

# struct component which simulates failures and repairs
mutable struct Repairable_component
    name::Any
    MTTF::Int64
    MTTR::Int64
    dutyCycle::Float64
    repair_count::Int64
    distribution_failure::Any
    distribution_repair::Any
    state_component::State
    MTTR_current::Float64
    MTBF_current::Float64
    mean_Up_Time::Float64
    failure_time::Float64
    repair_time::Float64
    between_failure_count::Int64
    MTTF_current::Float64
    failure_count::Int64
    up_time::Float64
end

# constructor given all variables
Repairable_component(name,MTTF,MTTR,dutyCycle, repair_count, distribution_failure,
 distribution_repair, state_component, MTTR_current, MTBF_current, mean_Up_Time, failure_time,
 repair_time, between_failure_count, MTTF_current, failure_count,up_time)= Repairable_component(name,MTTF*HOURS_TO_SECONDS,
MTTR*HOURS_TO_SECONDS, dutyCycle, 0, distribution_failure,
 distribution_repair, active, 0.0, 0.0, 0.0, 0.0, 0.0, 0, 0.0, 0, 0.0)

# constructor given only required variables
Repairable_component(name,MTTF,MTTR,dutyCycle)=
Repairable_component(name,MTTF*HOURS_TO_SECONDS, MTTR*HOURS_TO_SECONDS, dutyCycle,
 0, Distributions.Poisson(step/(MTTF*HOURS_TO_SECONDS*10)),
  Distributions.Exponential(MTTR*HOURS_TO_SECONDS), active, 0.0, 0.0, 0.0, 0.0, 0.0, 0, 0.0, 0, 0.0)
