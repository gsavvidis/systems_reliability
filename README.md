Systems Reliability

+ main_part1 includes the component and system simulation for component faults
+ Component includes the class component used for main_part1 simulation. It includes all the required component features.
+ System includes the class system used for main_part1 simulation. It includes all the required system features.

+ main_part2 includes the component and system simulation for component faults and repairs
+ Component includes the class component used for main_part2 simulation. It includes all the required component features.
+ System includes the class system used for main_part2 simulation. It includes all the required system features.

+ plots folder includes all extracted plots from main_part2
