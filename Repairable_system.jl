# enumeration to show the system state
@enum State active=1 inactive=2 faulty=3

# struct system which simulates failures and repairs
mutable struct Repairable_system
    failure_count::Int64
    state::State
    MTTR_current::Float64
    MTBF_current::Float64
    mean_Up_Time::Float64
    failure_time::Float64
    repair_time::Float64
    up_time::Float64
    repair_count::Int64
end

# constructor given only required variables
Repairable_system()=Repairable_system(0, active, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0)
