# enumeration to show the system state
@enum State active=1 inactive=2 faulty=3

# struct system which simulates failures
mutable struct System
    failure_count::Int64
    state::State
    MTTF_current::Float64
end

# constructor given only required variables
System(failure_count, state, MTTF_current)=System(0, active, 0.0)
