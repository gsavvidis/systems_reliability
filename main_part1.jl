# System Reliability Task 1 - Failure Simulation
# Importing libraries
import Distributions
using Random
using Printf
using ProgressBars

# Including required structs
include("Component.jl")
include("System.jl")

# Constants
tc = 100 #Component simulation time
ts = 30 #System simulation time
COMPONENT_TIME = tc * HOURS_TO_SECONDS #Component simulation time in seconds
N=1000 #Number of simulations
step=300 #Simulation timestep in seconds

#Components array initialization
components=[
    Component("C1",30,0.3),
    Component("C2",24,1.0),
    Component("C3",23,1.0),
    Component("C4",24,1.0),
    Component("C5",27,1.0),
    Component("C6",28,1.0),
    Component("C7",33,0.4),
]

# system initialization
system=System(0, active, 0.0)

# Number of simulations loop
for simulation_counter in 1:N
    # Time for each simulation loop
    for time in 0:step:(COMPONENT_TIME-1)
        # Duty cycle of components in each timestep
        for duty_cycle_time in 0:9
            # Iterate for all components
            for component_counter in components
                # When the component should switch from active to inactive
                # (based on its duty cycle) and it is not faulty
                if duty_cycle_time/10 == component_counter.dutyCycle &&
                     component_counter.state_component!=faulty
                    component_counter.state_component=inactive
                end
                # Run the failure variable based on the component distribution
                distribution_local=rand(component_counter.distribution,1)

                # If the component should become faulty (and it is active)
                if (distribution_local[1] > 0 && component_counter.state_component==active)
                    # Switch it to faulty
                    component_counter.state_component=faulty

                    # Calculate MTTF
                    component_counter.MTTF_current=(component_counter.MTTF_current*
                    component_counter.failure_count+time+duty_cycle_time*0.1)/
                    (component_counter.failure_count+1)

                    # Add to failure count
                    component_counter.failure_count=component_counter.failure_count+1
                end
            end
            # Based on the system diagramm, turn system into faulty
            if (components[1].state_component==faulty || (components[2].state_component==faulty &&
                components[3].state_component==faulty && components[4].state_component==faulty) ||
                (components[5].state_component==faulty && components[6].state_component==faulty) ||
                components[7].state_component==faulty) && (system.state!=faulty) &&
                (time<ts*HOURS_TO_SECONDS)
                # Switch system to faulty
                system.state=faulty

                # Calculate system MTTF
                system.MTTF_current=(system.MTTF_current*
                system.failure_count+time+duty_cycle_time*0.1)/
                (system.failure_count+1)

                # Add to failure count
                system.failure_count=system.failure_count+1
            end
        end
        # Iterate components and redo them as active every 1 timestep (duty cycle)
        for component_counter in components
            if component_counter.state_component==inactive
                component_counter.state_component=active
            end
        end
    end
    # Iterate components and redo them as active every simulation
    for component_counter in components
        component_counter.state_component=active
    end
    system.state=active
end
# Print component metrics
for component_counter in components
    component_counter.MTTF_current=component_counter.MTTF_current/HOURS_TO_SECONDS
    @printf(
            "Name:%s, λ=%.4f, MTTF=%.3f, R=%.6f\n",
            component_counter.name,
            1/component_counter.MTTF_current,
            component_counter.MTTF_current,
            exp(-COMPONENT_TIME/(component_counter.MTTF_current*HOURS_TO_SECONDS))
    )
end
# Print system metrics
system.MTTF_current=system.MTTF_current/HOURS_TO_SECONDS
@printf(
        "Name:System, λ=%.4f, MTTF=%.3f, R=%.6f\n",
        1/system.MTTF_current,
        system.MTTF_current,
        exp(-COMPONENT_TIME/(system.MTTF_current*HOURS_TO_SECONDS))
)
