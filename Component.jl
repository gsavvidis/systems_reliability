# enumeration to show the component state
@enum State active=1 inactive=2 faulty=3

HOURS_TO_SECONDS = 3600

# struct component which simulates failures
mutable struct Component
    name::Any
    MTTF::Int64
    dutyCycle::Float64
    failure_count::Int64
    distribution::Any
    state_component::State
    MTTF_current::Float64
end

# constructor given all variables
Component(name,MTTF,dutyCycle, failure_count, distribution, state_component, MTTF_current)=
Component(name,MTTF*HOURS_TO_SECONDS, dutyCycle, 0, distribution, active, 0.0)

# constructor given only required variables
Component(name,MTTF,dutyCycle)=
Component(name,MTTF*HOURS_TO_SECONDS, dutyCycle,
 0, Distributions.Poisson(step/(MTTF*HOURS_TO_SECONDS*10)), active, 0.0)
