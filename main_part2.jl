# System Reliability Task 2 - Failure and Repair Simulation
# Importing libraries
import Distributions
using Random
using Printf
using ProgressBars
using Plots
#using JuMP

# Including required structs
include("Repairable_component.jl")
include("Repairable_system.jl")

# Constants
tc = 100 #Component simulation time
ts = 30 #System simulation time
COMPONENT_TIME = tc * HOURS_TO_SECONDS #Component simulation time in seconds
N=100 #Number of simulations
step=300 #Simulation timestep in seconds

#Components array initialization
components=[
    Repairable_component("C1",30,12,0.3),
    Repairable_component("C2",24,12,1.0),
    Repairable_component("C3",23,12,1.0),
    Repairable_component("C4",24,10,1.0),
    Repairable_component("C5",27,10,1.0),
    Repairable_component("C6",28,8,1.0),
    Repairable_component("C7",33,12,0.4),
]

# Plot array initialization
plot_array=BitArray(undef,convert(Int64,(COMPONENT_TIME)/step),8)
color=Array{String}(undef,convert(Int64,(COMPONENT_TIME)/step),8)

# system initialization
system=Repairable_system()

# Number of simulations loop
for simulation_counter in 1:N
    # Time for each simulation loop
    for time in 0:step:(COMPONENT_TIME-1)
        # Duty cycle of components in each timestep
        for duty_cycle_time in 0:9
            # Iterate for all components
            for component_counter in components
                # When the component should switch from active to inactive
                # (based on its duty cycle) and it is not faulty
                if duty_cycle_time/10 == component_counter.dutyCycle &&
                     component_counter.state_component!=faulty
                    component_counter.state_component=inactive
                end
                # Repair the component if it is faulty and the required time has passed
                if component_counter.state_component==faulty &&
                    ((time+duty_cycle_time*0.1 - component_counter.failure_time) > component_counter.repair_time)
                    # Switch it to active
                    component_counter.state_component=active

                    # Calculate MTTR
                    component_counter.MTTR_current=(component_counter.MTTR_current*
                    component_counter.repair_count+component_counter.repair_time)/
                    (component_counter.repair_count+1)

                    # Add to repair count
                    component_counter.repair_count=component_counter.repair_count+1
                # If the component is active
                elseif component_counter.state_component==active
                    # Run the failure variable based on the component distribution
                    distribution_local_fail=rand(component_counter.distribution_failure,1)
                    # If it fails
                    if distribution_local_fail[1]>0
                        # Switch it to faulty
                        component_counter.state_component=faulty

                        ## Calculate MTBF
                        if component_counter.failure_time!=0.0
                            component_counter.MTBF_current=(component_counter.MTBF_current*
                            component_counter.between_failure_count+time+duty_cycle_time*0.1-
                            component_counter.failure_time)/(component_counter.between_failure_count+1)

                            component_counter.between_failure_count=component_counter.between_failure_count+1
                        end

                        ## Calculate MTTF
                        component_counter.MTTF_current=(component_counter.MTTF_current*
                        component_counter.failure_count+time+duty_cycle_time*0.1-component_counter.failure_time-
                        component_counter.repair_time)/(component_counter.failure_count+1)

                        # Add to failure count
                        component_counter.failure_count=component_counter.failure_count+1

                        # Calculate Up Time
                        component_counter.up_time=component_counter.up_time+time+duty_cycle_time*0.1-
                        component_counter.failure_time-component_counter.repair_time

                        # Update failure time
                        component_counter.failure_time=time+duty_cycle_time*0.1

                        ## Define the repair time based on the Component Repair Distribution
                        distribution_local_repair=rand(component_counter.distribution_repair,1)
                        component_counter.repair_time=distribution_local_repair[1]
                        #println(component_counter.name)
                        #println(component_counter.repair_time/HOURS_TO_SECONDS)
                    end
                end
            end
            # Based on the system diagramm, turn system into faulty
            if (components[1].state_component==faulty || (components[2].state_component==faulty &&
                components[3].state_component==faulty && components[4].state_component==faulty) ||
                (components[5].state_component==faulty && components[6].state_component==faulty) ||
                components[7].state_component==faulty) && (system.state!=faulty) &&
                (time<tc*HOURS_TO_SECONDS)
                # Switch system to faulty
                system.state=faulty

                # Calculate system MTBF
                system.MTBF_current=(system.MTBF_current*
                system.failure_count+time+duty_cycle_time*0.1-system.failure_time)/
                (system.failure_count+1)

                # Add to failure count
                system.failure_count=system.failure_count+1

                # Calculate system Up Time
                system.up_time=system.up_time+time+duty_cycle_time*0.1-system.repair_time

                # Update failure time
                system.failure_time=time+duty_cycle_time*0.1
            # Based on the system diagramm, repair the system
            elseif !((components[1].state_component==faulty || (components[2].state_component==faulty &&
                components[3].state_component==faulty && components[4].state_component==faulty) ||
                (components[5].state_component==faulty && components[6].state_component==faulty) ||
                components[7].state_component==faulty)) && (time<tc*HOURS_TO_SECONDS) && system.state==faulty
                # Switch system to active
                system.state=active

                # Calculate MTTR
                system.MTTR_current=(system.MTTR_current*
                system.repair_count+time+duty_cycle_time*0.1-system.failure_time)/
                (system.repair_count+1)

                # Update repair time
                system.repair_time=time+duty_cycle_time*0.1

                # Add to repair count
                system.repair_count=system.repair_count+1
            end
        end
        # For the first simulation calculate the required plots
        # Essentially we only plot the first simulation
        if simulation_counter ==1
            i=1
            # Iterate all components
            for component_counter in components
                # Set 1 if the state is faulty and 0 if not
                plot_array[convert(Int64,(time)/step+1),i]=(component_counter.state_component==faulty)
                # Set red color if the state is faulty and 0 if not
                color[convert(Int64,(time)/step+1),i]=if (component_counter.state_component==faulty) "red" else "gray" end
                i+=1
            end
            # Set 1 if the system state is faulty and 0 if not
            plot_array[convert(Int64,(time)/step+1),i]=(system.state==faulty)
            # Set red color if the system state is faulty and 0 if not
            color[convert(Int64,(time)/step+1),i]=if (system.state==faulty) "red" else "gray" end
        end
        # Iterate components and redo them as active every 1 timestep (duty cycle)
        for component_counter in components
            if component_counter.state_component==inactive
                component_counter.state_component=active
            end
        end
    end
    # Iterate components and redo them as active every simulation
    for component_counter in components
        if component_counter.state_component!=faulty # inactive state is considered as up_time
            # Update component Up Time at the end of each simulation
            component_counter.up_time=component_counter.up_time+COMPONENT_TIME+0.9-
            component_counter.failure_time-component_counter.repair_time
        end
        # Calculate component mean Up Time at the end of each simulation
        component_counter.mean_Up_Time=(component_counter.mean_Up_Time*
        (simulation_counter-1)+component_counter.up_time)/simulation_counter

        # Reset component variables
        component_counter.state_component=active
        component_counter.failure_time=0.0
        component_counter.repair_time=0.0
        component_counter.up_time=0.0
    end
    if system.state!=faulty # inactive state is considered up_time
        # Update system Up Time at the end of each simulation
        system.up_time=system.up_time+COMPONENT_TIME+0.9-system.repair_time
    end
    # Calculate system mean Up Time at the end of each simulation
    system.mean_Up_Time=(system.mean_Up_Time*
    (simulation_counter-1)+system.up_time)/simulation_counter

    # Resent system variables
    system.up_time=0.0
    system.repair_time=0.0
    system.failure_time=0.0
end

# Print component metrics
for component_counter in components
    component_counter.MTTR_current=component_counter.MTTR_current/HOURS_TO_SECONDS
    component_counter.MTBF_current=component_counter.MTBF_current/HOURS_TO_SECONDS
    component_counter.MTTF_current=component_counter.MTTF_current/HOURS_TO_SECONDS
    component_counter.mean_Up_Time=component_counter.mean_Up_Time/HOURS_TO_SECONDS
    @printf(
            "Name:%s, MTBF=%.4f, MTTR=%.3f, MTTF=%.3f, MTBF_alternative=%.4f, MUT=%.4f, A=%.6f\n",
            component_counter.name,
            component_counter.MTBF_current,
            component_counter.MTTR_current,
            component_counter.MTTF_current,
            component_counter.MTTF_current+component_counter.MTTR_current,
            component_counter.mean_Up_Time,
            component_counter.MTBF_current/(component_counter.MTBF_current+component_counter.MTTR_current)
    )
end

# Print system metrics
system.MTTR_current=system.MTTR_current/HOURS_TO_SECONDS
system.MTBF_current=system.MTBF_current/HOURS_TO_SECONDS
system.mean_Up_Time=system.mean_Up_Time/HOURS_TO_SECONDS
@printf(
        "Name:Repairable System, MTTR=%.4f, MTBF=%.4f, MUT=%.4f, A=%.4f",
        system.MTTR_current,
        system.MTBF_current,
        system.mean_Up_Time,
        system.MTBF_current/(system.MTBF_current+system.MTTR_current)
)

# Plot graphs for component and system states at any time during the first simulation
for counter in 1:7
    plot(1:step:COMPONENT_TIME,plot_array[:,counter],color=color[:,counter],label=components[counter].name)
    savefig("plots/plot$(components[counter].name).pdf")
end
plot(1:step:COMPONENT_TIME,plot_array[:,8],color=color[:,8],label="System")
savefig("plots/plotSystem.pdf")
